import React from 'react';
import { Platform, StyleSheet, Text, TouchableNativeFeedback, TouchableOpacity, View } from 'react-native';

const CategoryGridTile = props => {
    let TouchableComponent = TouchableOpacity;

    if(Platform.OS === 'android' && Platform.Version >= 21) {
        TouchableComponent = TouchableNativeFeedback;
    }

    return (
        <View style={styles.gridItem}>
            <TouchableComponent style={{ flex: 1 }} onPress={props.onSelect}>
                <View style={[styles.container, {backgroundColor: props.color}]}>
                    <Text style={styles.title} numberOfLines={2}>{props.title}</Text>
                </View>
            </TouchableComponent>
        </View>
    )
};

const styles = StyleSheet.create({
    gridItem: {
        flex: 1,
        margin: 15,
        height: 150,
        overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
    },
    title: {
        fontFamily: 'open-sans-bold',
        color: 'black',
        fontSize: 18,
        textAlign: 'right'
    },
    container: {
        flex: 1,
        padding: 10,
        borderRadius: 10,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    }
});

export default CategoryGridTile;