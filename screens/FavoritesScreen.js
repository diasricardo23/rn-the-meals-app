import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { useSelector } from 'react-redux';
import HeaderButton from '../components/HeaderButton';
import MealList from '../components/MealList';

const FavoritesScreen = props => {
    const favMeals = useSelector(state => state.meals.favoriteMeals);

    if(favMeals.length === 0 || !favMeals) {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>No favorite meals found. Start adding some!</Text>
            </View>
        )
    } else {
        return (
            <MealList listData={favMeals} navigation={props.navigation}/>
        );
    }
};

FavoritesScreen.navigationOptions = (navData) => {
    return {
        headerLeft: () => (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
              <Item
                  title="Menu"
                  iconName="ios-menu"
                  onPress={() => {
                      navData.navigation.toggleDrawer();
                  }}
              />
          </HeaderButtons>
        )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontFamily: 'open-sans',
        fontSize: 16,
        textAlign: 'center'
    }
});

export default FavoritesScreen;

