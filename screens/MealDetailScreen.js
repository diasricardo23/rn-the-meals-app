import React, { useCallback, useEffect } from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { useDispatch, useSelector } from 'react-redux';
import HeaderButton from '../components/HeaderButton';
import { toggleFavorite } from '../store/actions/meals';

const ListItem = props => {
    return (
        <View style={styles.listItem}>
            <Text style={styles.text}>{props.children}</Text>
        </View>
    );
};

const MealDetailScreen = props => {
    const mealId = props.navigation.getParam('mealId');
    const meals = useSelector(state => state.meals.meals);
    const isFavorite = useSelector(state =>
        state.meals.favoriteMeals.some(meal => meal.id === mealId
    ));
    const selectedItem = meals.find(meal => meal.id === mealId);

    const dispatch = useDispatch();

    const toggleFavoriteHandler = useCallback(() => {
        dispatch(toggleFavorite(mealId));
    }, [dispatch, mealId]);

    useEffect(() => {
        props.navigation.setParams({ toggleFav: toggleFavoriteHandler });
    }, [toggleFavoriteHandler]);

    useEffect(() => {
        props.navigation.setParams({ isFav: isFavorite });
    }, [isFavorite]);

    return (
        <ScrollView>
            <Image source={{uri: selectedItem.imageUrl}} style={styles.image}/>
            <View style={styles.details}>
                <Text style={styles.text}>{selectedItem.duration}m</Text>
                <Text style={styles.text}>{selectedItem.complexity.toUpperCase()}</Text>
                <Text style={styles.text}>{selectedItem.affordability.toUpperCase()}</Text>
            </View>
            <Text style={styles.title}>Ingredients</Text>
            {selectedItem.ingredients.map(ingredient => (
                <ListItem key={ingredient}>{ingredient}</ListItem>
            ))}
            <Text style={styles.title}>Steps</Text>
            {selectedItem.steps.map(step => (
                <ListItem key={step}>{step}</ListItem>
            ))}
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 200
    },
    details: {
        flexDirection: 'row',
        padding: 15,
        justifyContent: 'space-around'
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 22,
        textAlign: 'center'
    },
    text: {
        fontFamily: 'open-sans',
    },
    listItem: {
        marginVertical: 5,
        marginHorizontal: 20,
        borderColor: '#CCC',
        borderWidth: 1,
        padding: 5
    }
});

MealDetailScreen.navigationOptions = navigationData => {
    const mealTitle = navigationData.navigation.getParam('mealTitle');
    const isFavorite = navigationData.navigation.getParam('isFav');
    const toggleFavorite = navigationData.navigation.getParam('toggleFav');

    return {
        headerTitle: mealTitle,
        headerRight: () =>
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Favorite"
                    iconName={isFavorite ? 'ios-star' : 'ios-star-outline'}
                    onPress={toggleFavorite}
                />
            </HeaderButtons>
    };
};

export default MealDetailScreen;

