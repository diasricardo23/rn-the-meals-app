import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useSelector } from 'react-redux';
import MealList from '../components/MealList';
import { CATEGORIES } from '../data/dummy-data';

const CategoryMealsScreen = props => {
    const catId = props.navigation.getParam('categoryId');
    const availableMeals = useSelector(state => state.meals.filteredMeals);
    const displayedMeals = availableMeals.filter(
        meal => meal.categoryIds.indexOf(catId) >= 0
    );

    if(displayedMeals.length === 0) {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>No meals found, check your filters!</Text>
            </View>
        )
    } else {
        return (
            <MealList listData={displayedMeals} navigation={props.navigation}/>
        );
    }

};

CategoryMealsScreen.navigationOptions = navigationData => {
    const catId = navigationData.navigation.getParam('categoryId');
    const selectedItem = CATEGORIES.find(cat => cat.id === catId);

    return {
        headerTitle: selectedItem.title,
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontFamily: 'open-sans',
        fontSize: 16,
        textAlign: 'center'
    }
});

export default CategoryMealsScreen;

