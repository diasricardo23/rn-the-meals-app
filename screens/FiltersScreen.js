import React, { useCallback, useEffect, useState } from 'react';
import { Platform, StyleSheet, Switch, Text, View } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { useDispatch } from 'react-redux';
import HeaderButton from '../components/HeaderButton';
import Colors from '../constants/Colors';
import { setFilters } from '../store/actions/meals';

const SwitchContainer = props => {
    return (
        <View style={styles.filterContainer}>
            <Text>{props.label}</Text>
            <Switch
                value={props.state}
                trackColor={{ true: Colors.primaryColor }}
                thumbColor={Platform.OS === 'android' ? Colors.primaryColor : ''}
                onValueChange={props.onChange}
            />
        </View>
    )
}

const FiltersScreen = props => {
    const { navigation } = props;

    const [isGlutenFree, SetIsGlutenFree] = useState(false);
    const [isLactoseFree, SetIsLactoseFree] = useState(false);
    const [isVegan, SetIsVegan] = useState(false);
    const [isVegetarian, SetIsVegetarian] = useState(false);

    const dispatch = useDispatch();

    const saveFilters = useCallback(() => {
        const appliedFilters = {
            glutenFree: isGlutenFree,
            lactoseFree: isLactoseFree,
            vegan: isVegan,
            vegetarian: isVegetarian
        };

        dispatch(setFilters(appliedFilters));
    }, [isGlutenFree, isLactoseFree, isVegan, isVegetarian]);

    useEffect(() => {
        navigation.setParams({ save: saveFilters })
    }, [saveFilters])

    return (
        <View style={styles.screen}>
            <Text style={styles.title}>Available Filters / Restrictions</Text>
            <SwitchContainer label="Gluten-Free" state={isGlutenFree} onChange={newValue => SetIsGlutenFree(newValue)}/>
            <SwitchContainer label="Lactose-Free" state={isLactoseFree} onChange={newValue => SetIsLactoseFree(newValue)}/>
            <SwitchContainer label="Vegan" state={isVegan} onChange={newValue => SetIsVegan(newValue)}/>
            <SwitchContainer label="Vegetarian" state={isVegetarian} onChange={newValue => SetIsVegetarian(newValue)}/>
        </View>
    );
};

FiltersScreen.navigationOptions = (navData) => {
    return {
        headerLeft: () => (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
              <Item
                  title="Menu"
                  iconName="ios-menu"
                  onPress={() => {
                      navData.navigation.toggleDrawer();
                  }}
              />
          </HeaderButtons>
        ),
        headerRight: () => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Save"
                    iconName="ios-save"
                    onPress={navData.navigation.getParam('save')}
                />
            </HeaderButtons>
          )
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center'
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 22,
        margin: 20,
        textAlign: 'center'
    },
    filterContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '80%',
        marginVertical: 5
    }
});

export default FiltersScreen;

